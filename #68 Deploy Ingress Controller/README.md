Deploying an Ingress Controller in your Kubernetes cluster is a pivotal step toward making your applications accessible via human-friendly domain names rather than raw IP addresses. Ingress Controllers act as a reverse proxy and load balancer for your Kubernetes services, routing external HTTP(S) traffic to the appropriate internal services based on the requested host or URL.

## Minikube
For local development with Minikube, enabling the Ingress addon is straightforward and does not require installing Helm charts manually.

Enable the Ingress Addon:
Run the following command to enable the Ingress addon in Minikube:

```bash
minikube addons enable ingress
```

Verify the Ingress Controller:
Check that the Ingress controller pods are up and running:

```bash
kubectl get pods -n kube-system | grep ingress-nginx
```

## Linode Kubernetes Engine (LKE)
On cloud environments like LKE, deploying an Ingress Controller typically involves installing a Helm chart.

Add the Ingress-Nginx Helm Repository:
Start by adding the official Ingress-Nginx Helm repository and update your Helm repositories:

```bash
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update
```

Install the Ingress-Nginx Controller:
Use Helm to deploy the Ingress-Nginx controller:

```bash
helm install ingress-nginx ingress-nginx/ingress-nginx
```

This command deploys the Ingress-Nginx controller with default settings, which should suffice for most initial setups. For more advanced configurations, you may need to customize the installation with additional flags or a custom values file.

Verify the Installation:
After installation, ensure that the Ingress-Nginx pods are running:

```bash
kubectl get pods --namespace=default -l app.kubernetes.io/name=ingress-nginx
```

## Post-Deployment Steps
With the Ingress Controller deployed, the next step involves configuring Ingress resources for your applications. This configuration will define how incoming requests are routed to your services based on the request host or URL.

- Create Ingress Resources: Define Ingress resources in your Kubernetes manifests, specifying rules for routing traffic to your application services. These resources will utilize the Ingress Controller to manage external access to your services.

- Configure DNS: To use domain names with your applications, you'll need to configure DNS records pointing to your Ingress Controller's external IP address. For Minikube, you'll typically test with local host file modifications, while for LKE, you can obtain the external IP using kubectl get services --namespace=default -l app.kubernetes.io/name=ingress-nginx and then configure your DNS provider accordingly.

Deploying an Ingress Controller and configuring Ingress resources are crucial steps in exposing your Kubernetes-managed applications to the outside world in a scalable and maintainable manner.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/container-orchestration-with-kubernetes/-/blob/main/LICENSE.md?ref_type=heads)

# Author
Adetayo Michael Ibijemilusi
