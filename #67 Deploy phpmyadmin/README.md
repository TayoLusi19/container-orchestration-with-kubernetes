To deploy phpMyAdmin in your Kubernetes cluster for easy MySQL database management through a web interface, you'll create a simple deployment and an internal service. This setup ensures that phpMyAdmin is accessible within your cluster, allowing you to manage your MySQL databases securely.

## Creating the phpmyadmin.yaml Manifest
Below is a basic template for the phpmyadmin.yaml file. This template includes a Deployment and a Service for phpMyAdmin. Ensure that you replace <mysql-service-name> with the name of your MySQL service within Kubernetes so phpMyAdmin can connect to it.

```bash
apiVersion: apps/v1
kind: Deployment
metadata:
  name: phpmyadmin
spec:
  replicas: 1
  selector:
    matchLabels:
      app: phpmyadmin
  template:
    metadata:
      labels:
        app: phpmyadmin
    spec:
      containers:
      - name: phpmyadmin
        image: phpmyadmin/phpmyadmin
        env:
        - name: PMA_HOST
          value: "<mysql-service-name>"
        ports:
        - containerPort: 80
---
apiVersion: v1
kind: Service
metadata:
  name: phpmyadmin
spec:
  selector:
    app: phpmyadmin
  ports:
  - port: 80
    targetPort: 80
```

This configuration does the following:

- Deployment: Creates a phpMyAdmin pod (1 replica) that connects to your MySQL database. The PMA_HOST environment variable is crucial as it tells phpMyAdmin where to find the MySQL server. Replace <mysql-service-name> with the actual service name of your MySQL deployment.
- Service: Exposes phpMyAdmin within the cluster using an internal service. This means phpMyAdmin can be accessed from within the cluster at phpmyadmin:80.

## Deploying phpMyAdmin
1. Save the above configuration to a file named phpmyadmin.yaml.
2. Apply the configuration to your Kubernetes cluster:

```bash
kubectl apply -f phpmyadmin.yaml
```

## Accessing phpMyAdmin
- On Minikube: You can make phpMyAdmin accessible via a browser by running minikube service phpmyadmin. Minikube will open a browser window with the phpMyAdmin interface, allowing you to manage your MySQL database.

- On LKE or Other Non-local Kubernetes Clusters: Since the service is internal, you can access phpMyAdmin through port forwarding. Run kubectl port-forward svc/phpmyadmin 8080:80 and access phpMyAdmin by visiting http://localhost:8080 in your browser.

Remember to secure your phpMyAdmin deployment, especially if you plan to change the service to be externally accessible. Use Kubernetes Secrets to manage sensitive information and consider implementing additional security measures like IP whitelisting and authentication layers.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/container-orchestration-with-kubernetes/-/blob/main/LICENSE.md?ref_type=heads)

# Author
Adetayo Michael Ibijemilusi
