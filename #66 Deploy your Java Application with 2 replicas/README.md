Deploying your Java application on Kubernetes with two replicas involves creating Kubernetes objects such as Deployments, ConfigMaps, and Secrets. ConfigMaps and Secrets allow you to manage configuration and sensitive data separately from your application code. Below, I'll guide you through the process tailored for both Minikube and Linode Kubernetes Engine (LKE) environments.

## Step 1: Creating a Docker Registry Secret
To pull a private image from Docker Hub, you'll need to create a Kubernetes secret with your Docker credentials. Replace the placeholders with your Docker Hub username, password, and email:

```bash
DOCKER_REGISTRY_SERVER=docker.io
DOCKER_USER=<your_dockerID>
DOCKER_EMAIL=<your_dockerhub_email>
DOCKER_PASSWORD=<your_dockerhub_password>

kubectl create secret docker-registry my-registry-key \
--docker-server=$DOCKER_REGISTRY_SERVER \
--docker-username=$DOCKER_USER \
--docker-password=$DOCKER_PASSWORD \
--docker-email=$DOCKER_EMAIL
```

## Step 2: Deploying ConfigMap and Secret
Before deploying your application, create a ConfigMap and Secret to store your application's configuration and sensitive data:

- ConfigMap (db-config.yaml): Contains non-sensitive data, like database URL or application settings.
- Secret (db-secret.yaml): Stores sensitive data, like database username and password.
Ensure these files are correctly populated with your specific configuration and sensitive data. Then, apply them:

```bash
kubectl apply -f db-secret.yaml
kubectl apply -f db-config.yaml
```

## Step 3: Deploying Your Java Application
With the java-app.yaml Kubernetes manifest file, you'll define a Deployment to manage your application instances and a Service to expose them. Ensure that your java-app.yaml references the ConfigMap and Secret you've created and is set up to use the image nanatwn/demo-app:mysql-app.

Here's a basic structure for your java-app.yaml (ensure to adjust according to your application's specific needs):

```bash
apiVersion: apps/v1
kind: Deployment
metadata:
  name: java-app-deployment
spec:
  replicas: 2
  selector:
    matchLabels:
      app: java-app
  template:
    metadata:
      labels:
        app: java-app
    spec:
      containers:
      - name: java-app
        image: nanatwn/demo-app:mysql-app
        ports:
        - containerPort: 8080
        envFrom:
        - configMapRef:
            name: db-config
        - secretRef:
            name: db-secret
      imagePullSecrets:
      - name: my-registry-key
---
apiVersion: v1
kind: Service
metadata:
  name: java-app-service
spec:
  selector:
    app: java-app
  ports:
  - protocol: TCP
    port: 80
    targetPort: 8080
  type: LoadBalancer
```

This manifest defines a Deployment for running 2 replicas of your Java application, referencing both the ConfigMap and Secret for environment variables, and a Service to expose your application.

## Step 4: Applying Your Application Manifest
From the k8s-deployment folder, apply your application configuration:

```bash
kubectl apply -f java-app.yaml
```

## Step 5: Verifying the Deployment
Check the status of your deployment and ensure your pods are running:

```bash
kubectl get deployments
kubectl get pods
```

To access your application, if you're using Minikube, you might use minikube service java-app-service to get the URL. For LKE, the service type LoadBalancer provides you with an external IP to access your application.

By following these steps, your Java application will be running on Kubernetes with high availability and scalability features. Kubernetes ensures that if a pod fails, it's replaced automatically, improving the reliability of your application.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/container-orchestration-with-kubernetes/-/blob/main/LICENSE.md?ref_type=heads)

# Author
Adetayo Michael Ibijemilusi
