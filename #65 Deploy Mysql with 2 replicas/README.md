To deploy a MySQL database with 2 replicas and ensure data persistence within your Kubernetes cluster, using Helm is indeed a streamlined approach. Helm simplifies Kubernetes application deployment by packaging applications as charts, which are collections of pre-configured Kubernetes resources. For this exercise, you will use the MySQL chart provided by Bitnami, which is well-maintained and widely used in the community.

### Steps to Deploy MySQL Database Using Helm
Prerequisites
Ensure you have the following installed and configured:

- kubectl: Configured to communicate with your Kubernetes cluster.
- Helm: The package manager for Kubernetes.
- Minikube or access to an LKE cluster: Depending on your setup.

Deployment Instructions

1. Clone the Repository:
First, clone the repository containing the Kubernetes manifest files and switch to the appropriate branch and directory as instructed:

```bash
git clone https://gitlab.com/twn-devops-bootcamp/latest/10-kubernetes/kubernetes-exercises.git
cd kubernetes-exercises
git checkout feature/solutions
cd k8s-deployment
```

2. Add Bitnami Helm Repository:
Bitnami provides a collection of Helm charts for popular open-source applications. Add the Bitnami repository to Helm:

```bash
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update
```

3. Deploy MySQL with Helm:
For Minikube, use the mysql-chart-values-minikube.yaml values file provided in the repository you cloned:

```bash
helm install my-release bitnami/mysql -f mysql-chart-values-minikube.yaml
```

For Linode Kubernetes Engine (LKE), use the mysql-chart-values-lke.yaml values file:

```bash
helm install my-release bitnami/mysql -f mysql-chart-values-lke.yaml
```

These commands deploy MySQL on the Kubernetes cluster in the default configuration. The my-release is the release name for your deployment which you can customize. The -f flag specifies a custom values file that overrides the settings from the default chart, such as the number of replicas and persistent volume configurations.

4. Verify the Deployment:
After deploying, you can verify that your MySQL instances are running:

```bash
kubectl get pods
```

You should see 2 replicas of MySQL pods running if the deployment was successful.

## General Notes
- Persistence: The MySQL chart is configured to use persistent volumes by default, which means your data will be persisted even if the pods are restarted or deleted.
- Configuration: The custom values file (mysql-chart-values-minikube.yaml or mysql-chart-values-lke.yaml) contains specific configurations like the number of replicas, persistent volume sizes, and more. You can edit this file to suit your requirements.
- Security: Make sure to review the security configurations, including the MySQL root password and other user credentials, within the values file or set them via Helm command line flags for enhanced security.

By following these steps, you will have a highly available MySQL database running on your Kubernetes cluster, leveraging Helm for easy deployment and management.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/container-orchestration-with-kubernetes/-/blob/main/LICENSE.md?ref_type=heads)

# Author
Adetayo Michael Ibijemilusi
