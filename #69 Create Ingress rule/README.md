
Creating an Ingress rule is a key step in configuring external access to your applications running on Kubernetes via a friendly URL. This involves defining an Ingress resource that specifies how inbound requests to a specific hostname should be routed to your application.

Below, I'll outline the steps for configuring an Ingress rule for both Minikube (for local testing) and Linode Kubernetes Engine (LKE) environments. You'll create an Ingress resource to make your application accessible at my-java-app.com.

## Minikube
Update Your Ingress Resource File (java-app-ingress.yaml):
Here's an example snippet to include in your java-app-ingress.yaml, adjusting the hostname to my-java-app.com:

```bash
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: java-app-ingress
spec:
  rules:
  - host: my-java-app.com
    http:
      paths:
      - pathType: Prefix
        path: "/"
        backend:
          service:
            name: java-app-service
            port:
              number: 80
```

Update Your Hosts File:
Add an entry to your /etc/hosts file to resolve my-java-app.com to 127.0.0.1 (localhost):

```bash
127.0.0.1 my-java-app.com
```

This step simulates DNS resolution for local testing.

Create the Ingress Resource:
Apply your Ingress configuration:

```bash
kubectl apply -f java-app-ingress.yaml
```

Run Minikube Tunnel:
Open a new terminal window and run the following command to enable external access to the services exposed by your Ingress:

```bash
minikube tunnel
```

Access Your Application:
Now, you can access your application in a browser using http://my-java-app.com.

## Linode Kubernetes Engine (LKE)
Update Your Ingress Resource File (java-app-ingress.yaml):
If you're using LKE, you'll point the hostname in your java-app-ingress.yaml file to your Linode NodeBalancer's external IP address or a domain name that resolves to it. For a domain, you can use an A record in your DNS settings to point to the external IP.

For LKE, you typically obtain an external IP for your Ingress Controller service that you would use as the A record target or directly in the browser. The java-app-ingress.yaml doesn't need to change unless you're setting it to a specific domain name.

Create the Ingress Resource:
Just like with Minikube, apply your Ingress configuration in the LKE environment:

```bash
kubectl apply -f java-app-ingress.yaml
```

Access Your Application:
- If using an external IP: Directly access your application by navigating to http://<external-ip> in your browser.
- If using a domain name: Access your application via the domain name (e.g., http://my-java-app.com) after DNS propagation.

For both environments, creating an Ingress rule is an efficient way to expose your application to the outside world using a friendly URL, leveraging Kubernetes' powerful networking capabilities.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/container-orchestration-with-kubernetes/-/blob/main/LICENSE.md?ref_type=heads)

# Author
Adetayo Michael Ibijemilusi
