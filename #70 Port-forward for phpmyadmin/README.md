Configuring port forwarding for your phpMyAdmin service is a secure method to access it locally without exposing it to the internet. This technique is particularly useful for administrative tasks on your MySQL database while keeping the phpMyAdmin interface inaccessible from outside your local machine. The process is the same for both Minikube and Linode Kubernetes Engine (LKE) or any Kubernetes cluster.

Before proceeding, ensure you have a service named phpmyadmin-service that exposes phpMyAdmin, typically on port 80 within your cluster. The command provided in the exercise assumes phpMyAdmin is running on port 8081, but typically phpMyAdmin inside Kubernetes would run on port 80. You might need to adjust the port-forwarding command accordingly if your service uses a different port.

## Step-by-Step Guide to Configure Port Forwarding
1. Identify Your phpMyAdmin Service: Confirm the name and port of your phpMyAdmin service within Kubernetes. If you're not sure, you can list all services with:

```bash
kubectl get svc
```

Look for phpmyadmin-service or the relevant service name in the output.

2. Configure Port Forwarding: The command format for port forwarding is kubectl port-forward svc/<service-name> <local-port>:<service-port>. For a typical phpMyAdmin deployment that listens on port 80, and if you want to access it on your local machine using port 8081, the command would be:

```bash
kubectl port-forward svc/phpmyadmin-service 8081:80
```

This command maps port 8081 on your local machine to port 80 of the phpmyadmin-service, allowing you to access phpMyAdmin by navigating to http://localhost:8081 in your web browser.

3. Access phpMyAdmin Locally: Open your preferred web browser and go to http://localhost:8081. You should see the phpMyAdmin login screen, where you can authenticate using your MySQL credentials.

4. Terminate Port Forwarding: When you're done using phpMyAdmin, you can stop the port forwarding by simply terminating the command in the terminal with Ctrl + C.

## Security Consideration
Port forwarding is a secure way to access cluster-internal services like phpMyAdmin without exposing them to the public internet. It's particularly useful for development and administrative tasks. However, always ensure your phpMyAdmin and database credentials are strong and kept secure to prevent unauthorized access.

By following these steps, you effectively set up a secure, temporary, local access to your phpMyAdmin service for database management tasks.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/container-orchestration-with-kubernetes/-/blob/main/LICENSE.md?ref_type=heads)

# Author
Adetayo Michael Ibijemilusi



























